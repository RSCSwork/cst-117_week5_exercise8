﻿namespace Exercise_8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateBtn = new System.Windows.Forms.Button();
            this.fatTxtBx = new System.Windows.Forms.TextBox();
            this.carbTxtBx = new System.Windows.Forms.TextBox();
            this.fatGramLbl = new System.Windows.Forms.Label();
            this.carbGramLbl = new System.Windows.Forms.Label();
            this.fatExplainLbl = new System.Windows.Forms.Label();
            this.carbExplainLbl = new System.Windows.Forms.Label();
            this.fatCaloriesLbl = new System.Windows.Forms.Label();
            this.carbCaloriesLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // calculateBtn
            // 
            this.calculateBtn.Location = new System.Drawing.Point(579, 49);
            this.calculateBtn.Name = "calculateBtn";
            this.calculateBtn.Size = new System.Drawing.Size(109, 70);
            this.calculateBtn.TabIndex = 0;
            this.calculateBtn.Text = "Calculate Both";
            this.calculateBtn.UseVisualStyleBackColor = true;
            this.calculateBtn.Click += new System.EventHandler(this.CalculateBtn_Click);
            // 
            // fatTxtBx
            // 
            this.fatTxtBx.Location = new System.Drawing.Point(426, 49);
            this.fatTxtBx.Name = "fatTxtBx";
            this.fatTxtBx.Size = new System.Drawing.Size(100, 26);
            this.fatTxtBx.TabIndex = 1;
            // 
            // carbTxtBx
            // 
            this.carbTxtBx.Location = new System.Drawing.Point(426, 93);
            this.carbTxtBx.Name = "carbTxtBx";
            this.carbTxtBx.Size = new System.Drawing.Size(100, 26);
            this.carbTxtBx.TabIndex = 2;
            // 
            // fatGramLbl
            // 
            this.fatGramLbl.AutoSize = true;
            this.fatGramLbl.Location = new System.Drawing.Point(32, 52);
            this.fatGramLbl.Name = "fatGramLbl";
            this.fatGramLbl.Size = new System.Drawing.Size(276, 20);
            this.fatGramLbl.TabIndex = 3;
            this.fatGramLbl.Text = "Please enter the number of fat grams:";
            // 
            // carbGramLbl
            // 
            this.carbGramLbl.AutoSize = true;
            this.carbGramLbl.Location = new System.Drawing.Point(32, 96);
            this.carbGramLbl.Name = "carbGramLbl";
            this.carbGramLbl.Size = new System.Drawing.Size(350, 20);
            this.carbGramLbl.TabIndex = 4;
            this.carbGramLbl.Text = "Please enter the number of carbohydrate grams:";
            // 
            // fatExplainLbl
            // 
            this.fatExplainLbl.AutoSize = true;
            this.fatExplainLbl.Location = new System.Drawing.Point(32, 216);
            this.fatExplainLbl.Name = "fatExplainLbl";
            this.fatExplainLbl.Size = new System.Drawing.Size(409, 20);
            this.fatExplainLbl.TabIndex = 5;
            this.fatExplainLbl.Text = "You have consumed this many calories in fat for one day:";
            // 
            // carbExplainLbl
            // 
            this.carbExplainLbl.AutoSize = true;
            this.carbExplainLbl.Location = new System.Drawing.Point(32, 284);
            this.carbExplainLbl.Name = "carbExplainLbl";
            this.carbExplainLbl.Size = new System.Drawing.Size(495, 20);
            this.carbExplainLbl.TabIndex = 6;
            this.carbExplainLbl.Text = "You have consumed this many calories in carbohydrates for one day: ";
            // 
            // fatCaloriesLbl
            // 
            this.fatCaloriesLbl.AutoSize = true;
            this.fatCaloriesLbl.Location = new System.Drawing.Point(604, 216);
            this.fatCaloriesLbl.Name = "fatCaloriesLbl";
            this.fatCaloriesLbl.Size = new System.Drawing.Size(0, 20);
            this.fatCaloriesLbl.TabIndex = 7;
            // 
            // carbCaloriesLbl
            // 
            this.carbCaloriesLbl.AutoSize = true;
            this.carbCaloriesLbl.Location = new System.Drawing.Point(604, 284);
            this.carbCaloriesLbl.Name = "carbCaloriesLbl";
            this.carbCaloriesLbl.Size = new System.Drawing.Size(0, 20);
            this.carbCaloriesLbl.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.carbCaloriesLbl);
            this.Controls.Add(this.fatCaloriesLbl);
            this.Controls.Add(this.carbExplainLbl);
            this.Controls.Add(this.fatExplainLbl);
            this.Controls.Add(this.carbGramLbl);
            this.Controls.Add(this.fatGramLbl);
            this.Controls.Add(this.carbTxtBx);
            this.Controls.Add(this.fatTxtBx);
            this.Controls.Add(this.calculateBtn);
            this.Name = "Form1";
            this.Text = "Calories from Fat and Carbohydrates";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateBtn;
        private System.Windows.Forms.TextBox fatTxtBx;
        private System.Windows.Forms.TextBox carbTxtBx;
        private System.Windows.Forms.Label fatGramLbl;
        private System.Windows.Forms.Label carbGramLbl;
        private System.Windows.Forms.Label fatExplainLbl;
        private System.Windows.Forms.Label carbExplainLbl;
        private System.Windows.Forms.Label fatCaloriesLbl;
        private System.Windows.Forms.Label carbCaloriesLbl;
    }
}

