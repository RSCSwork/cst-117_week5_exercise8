﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * Rikk Shimizu
 * This is my own work.
 */


namespace Exercise_8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }//ends form1

        /*
         * This is a method for the calculate button which will use the methods that follow
         * it, to return the number of calories eaten from fat and carbs to the user. 
         */
        private void CalculateBtn_Click(object sender, EventArgs e)
        {
            double amountOfFatInGrams, amountOfCarbsInGrams;

            if(!String.IsNullOrEmpty(fatTxtBx.Text) && !String.IsNullOrEmpty(carbTxtBx.Text))
            {
                if(double.TryParse(fatTxtBx.Text, out amountOfFatInGrams) && double.TryParse(carbTxtBx.Text, out amountOfCarbsInGrams))
                {
                    fatCaloriesLbl.Text = FatCalories(amountOfFatInGrams).ToString() + " cals";
                    carbCaloriesLbl.Text = CarbCalories(amountOfCarbsInGrams).ToString() + " cals";

                }//ends if to tryparse text to doubles

            }//ends check of text boxes to have valid information

        }//ends calculate btn

        /*
         * This method takes in a double for the number of grams of fat eaten in one day
         * and returns the number of calories for that many grams.
         * The use of doubles makes more sense than an int because the person could eat a 
         * fraction of a gram worth of fat. If the person went off a bag label then they may 
         * give the trainer a decimal number, or if the person weighs their food on a scale 
         * for something such as an oil which is a fat. 
         * 
         * @param double fatInGrams
         * @return double caloriesInFat
         */
        public double FatCalories(double fatInGrams)
        {
            //Do the calculation straight away during declaration, based on the ask in question 4 this was all the method needed to do.
            double caloriesInFat = fatInGrams * 9;

            return caloriesInFat;
        }//ends Fat Calories method


        /*
         * This method takes in a double for the number of grams of carbs eaten in one day
         * and returns the number of calories for that many grams.
         * The use of doubles makes more sense than an int because the person could eat a 
         * fraction of a gram worth of carbs. If the person went off a bag label then they may 
         * give the trainer a decimal number, or if the person weighs their food on a scale 
         * the scale may give them a floating number. 
         * 
         * @param double carbsInGrams
         * @return double caloriesInCarbs
         */
        public double CarbCalories(double carbsInGrams)
        {
            //do the calculation straight away during declaration, based on the ask in question 5 this was all the method needed to do.
            double caloriesInCarbs = carbsInGrams * 4;

            return caloriesInCarbs;
        }//ends Carbs Calories method



    }//ends class 
}//ends namespace
